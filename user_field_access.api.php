<?php

/**
 * Implemneation of hook_user_field_access_info_alter
 * 
 * Can be used to change icons and captions
 * @param array $infos
 * @return string 
 */
function hook_user_relationships_user_field_access_info_alter(&$infos){
  $infos['public']['label'] = "Bad Luck Brian";
  return $infos;
}


/**
 * Impemnetation of hook_user_field_access_info()
 * the key is the realm 
 * @return array
 *   An array with field definitions keyed by realm 
 */
function hook_user_field_access_info() {
  $info = array();
  $info['my_module'] = array(
    'label' => t('My Bitches'),
    'icon' => '/' . drupal_get_path('module', 'mymodule') . '/bitch.gif',
    'description' => t("My bitches can see it"),
  //  'access callback' => 'my_custom_callback_name' default callback name is used if this is left out
  );
  return $info;
}

/**
 * Sample default callback for realm my_module
 * @param type $field the fiedl being accessed
 * @param type $uid the uid of the account being accessed
 * @param type $account the acting user
 * @return boolean 
 */
function user_field_access_access_my_module($field, $uid, $account){
  
  return my_module_account_is_bitch_of_uid($uid, $account);
}