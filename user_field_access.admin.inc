<?php


/**
 * @file
 * User Relationships admin settings and config forms
 */

/**
 * Main settings
 */
function user_relationships_admin_settings() {

  $form = array();
  $form['general'] = array(
    '#type'   => 'fieldset',
    '#title'  => t('General'),
    '#weight' => -10,
    '#group' => 'settings',
  );
  $form['general']['user_field_access_default_realm'] = array(
    '#type'           => 'radios',
    '#title'          => t('Default realm'),
    '#description'    => t('The default realm for new fields.'),
    '#options' => user_field_access_options(),
    '#default_value'  => variable_get('user_field_access_default_realm', 'public'),
  );
  return system_settings_form($form);
}
